#!/bin/bash

BASEDIR=$(dirname $0)

# check args
if [[ $# -eq 0 ]] ; then
    echo 'no basefilename supplied!'
    exit 1
fi

echo "converting base with filename $1"

# copy last 10MB of base
# 1.5MB is not enough for calculation
tail -c 3000000 $BASEDIR/raw/base.ubx > $BASEDIR/rinex/$1.ubx
#cp $BASEDIR/raw/base.ubx $BASEDIR/rinex/$1.ubx
$BASEDIR/convbin -od -os -oi -ot -f 1 $BASEDIR/rinex/$1.ubx
