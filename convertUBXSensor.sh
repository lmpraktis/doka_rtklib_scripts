#!/bin/bash

BASEDIR=$(dirname $0)

# check args
if [[ $# -eq 0 ]] ; then
    echo 'no sensorinput supplied!'
    exit 1
fi

# check args
if [[ $# -eq 1 ]] ; then
    echo 'no sensoroutput supplied!'
    exit 1
fi

echo "converting sensor input file $1 to output $2"

cat $BASEDIR/raw/$1.ubx > $BASEDIR/rinex/$2.ubx
$BASEDIR/convbin -od -os -oi -ot -f 1 $BASEDIR/rinex/$2.ubx
