#!/bin/bash

BASEDIR=$(dirname $0)

# check args
if [[ $# -eq 0 ]] ; then
    echo 'no sensorfilename supplied!'
    exit 1
fi

# check args
if [[ $# -eq 1 ]] ; then
    echo 'no basefilename supplied!'
    exit 1
fi

sensorfilename=$1
basefilename=$2
exclsats=$3

echo "calculatePositions script with sensorfilename: $sensorfilename and basename: $basefilename"
echo "exclude satellites: $exclsats"

# set new excluded satellites
TARGET_KEY=pos1-exclsats
sed -i "s/\($TARGET_KEY *= *\).*/\1$exclsats/" $BASEDIR/options.conf

# calculate positions, for every observation a position is calculated
#output in long/lat
$BASEDIR/rnx2rtkp -k $BASEDIR/options.conf -o $BASEDIR/rinex/$sensorfilename.pos $BASEDIR/rinex/$sensorfilename.obs $BASEDIR/rinex/$basefilename.obs $BASEDIR/rinex/$basefilename.nav $BASEDIR/rinex/$basefilename.hnav $BASEDIR/rinex/$basefilename.gnav $BASEDIR/rinex/$basefilename.qnav $BASEDIR/rinex/$basefilename.lnav

# read most accurate (i.e. last) position from pos-file
posLine=$( tail -n 1 $BASEDIR/rinex/$sensorfilename.pos )  
IFS=', ' read -r -a results <<< "$posLine"
posE=${results[2]}
posN=${results[3]}
posU=${results[4]}

#echo ""
#echo ""

# echo results
echo "$posE"
echo "$posN"
echo "$posU"
